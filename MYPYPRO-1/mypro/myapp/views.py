from django.shortcuts import render
from django.http import HttpResponse , HttpResponseRedirect ,JsonResponse
from myapp.models import ContactForm , RegisterForm , UserDetails
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.core.mail import EmailMessage

# Create your views here.
def index(request):
    if 'id' in request.GET:
        id_ = request.GET['id']
        getSinigleValueQuery = ContactForm.objects.filter(id=id_).all()
        return render(request,'edit_details.html',{'editDetails':getSinigleValueQuery})
    if 'delId' in request.GET:
        id_ = request.GET['delId']
        getSinigleValueQuery = ContactForm.objects.filter(id=id_).all()
        getSinigleValueQuery.delete()
        messages.add_message(request, messages.INFO, 'Hello world.')
        return HttpResponseRedirect('/')
    if 'name' in request.POST:
        n_ = request.POST['name']
        e_ = request.POST['email']
        no_ = request.POST['number']
        m_ = request.POST['message']
        insertQuery = ContactForm()
        insertQuery.name = n_
        insertQuery.email = e_
        insertQuery.contact = no_
        insertQuery.message = m_
        insertQuery.save() 
    getData = ContactForm.objects.all()
    return render(request,'index.html',{'data':getData})

def anFile(request):
    return render(request,'base.html')

def updateDetails(request):
    updateQuery = ContactForm.objects.get(id=request.POST['id_'])
    updateQuery.name = request.POST['name']
    updateQuery.email = request.POST['email']
    updateQuery.contact = request.POST['contact']
    updateQuery.save() 
    return HttpResponseRedirect('/')


# new code
def register(r):
    if 'reg' in r.POST:
        un = r.POST['username']
        em = r.POST['email']
        no = r.POST['number']
        ps = r.POST['password']
        cps = r.POST['conf_password']
        # return HttpResponse(r.POST.items())
        # match password
        if cps != ps:
            messages.add_message(r,messages.INFO,'Password are not same')
            return HttpResponseRedirect('reg')
        # Creating Active User
        # userdata = User.objects.create_user(username,email,password)
        userdata = User.objects.create_user(em,em,ps)
        # userdata.first_name = 
        # userdata.last_name = 
        # inserting data
        data = RegisterForm(user = userdata,username=un , email = em,number=no,password=ps)
        if 'pro_img' in r.FILES:
            pro_img = r.FILES['pro_img']
            data.profile_img = pro_img
            data.save()
        data.save()
        # Email
        sub = 'This is subject'
        msz = 'This is message'
        em = EmailMessage(sub,msz,to=[em])
        em.send()
        messages.add_message(r,messages.INFO,'Inserted')
        return HttpResponseRedirect('/reg')
    # return HttpResponse(r.POST.items())
    if 'login' in r.POST:
        em = r.POST['email']
        ps = r.POST['password']
        user = authenticate(username=em,password=ps)
        if(user):
            login(r,user)
            messages.add_message(r,messages.INFO,'Login successfully')
            resp = HttpResponseRedirect('/')
            resp.set_cookie('user_id',user.id)
            resp.set_cookie('user_email',user.email)
            
            return resp

            # return HttpResponseRedirect('/')
        else:
            messages.add_message(r,messages.INFO,'Login unsuccessfull')
            return HttpResponseRedirect('/')
    return render(r,'register.html')

def logout_link(r):
    logout(r)
    resp = HttpResponseRedirect('/')
    resp.delete_cookie('user_email')
    resp.delete_cookie('user_id')
    return resp

def dashboard(r):
    loginData = RegisterForm.objects.filter(email=r.COOKIES.get('user_email')).values()
    return render(r,'dashboard.html',{'data':loginData})

def details(r):
    if 'getDetails' in r.GET:
        getLoginUserData = RegisterForm.objects.get(email=r.COOKIES.get('user_email'))
        getData = list(UserDetails.objects.filter(user_id = getLoginUserData).values())
        return JsonResponse(getData,safe=False)

    getLoginUserDetails = RegisterForm.objects.get(email=r.COOKIES.get('user_email'))
    insertDetails = UserDetails(user_id = getLoginUserDetails ,age = r.POST['a'] ,gender= r.POST['g'] ,city= r.POST['c'] ,state= r.POST['s'] ,pincode=r.POST['p'])
    insertDetails.save()
    return HttpResponse('1')
    
def json_page(r):
    return render(r,'rest_countries_json.html')