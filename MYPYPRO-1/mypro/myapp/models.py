from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class ContactForm(models.Model):
    name        = models.CharField(max_length=250)
    email       = models.CharField(max_length=250)
    contact     = models.IntegerField()
    message     = models.TextField()
    created_on  = models.DateTimeField(auto_now_add=True) 

class RegisterForm(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    username = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    number = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    profile_img = models.ImageField(upload_to='profile_img/%Y/%M/%d')

    def __str__(self):
        return self.email

class UserDetails(models.Model):
    user_id = models.ForeignKey(RegisterForm,on_delete=models.CASCADE)
    age = models.IntegerField()
    gender = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    pincode = models.IntegerField()

