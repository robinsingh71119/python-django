from django.contrib import admin
from myapp.models import ContactForm ,RegisterForm , UserDetails
# Register your models here.
class AdminContactForm(admin.ModelAdmin):
    list_display = ['name','email','message']
    list_filter = ['created_on']

class AdminRegisterForm(admin.ModelAdmin):
    list_display = ['username','email','number']
    list_filter = ['email']

class AdminUserDetails(admin.ModelAdmin):
    list_display = ['user_id','age','gender']
    list_filter = ['gender','city']

admin.site.register(ContactForm,AdminContactForm)
admin.site.register(RegisterForm,AdminRegisterForm)
admin.site.register(UserDetails,AdminUserDetails)