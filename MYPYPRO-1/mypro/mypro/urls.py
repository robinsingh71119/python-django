"""mypro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from myapp import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index,name='index'),
    path('abc/', views.anFile,name='anyfile'),
    path('upD/', views.updateDetails,name='updateDetails'),
    path('reg/', views.register,name='registerView'),
    path('dashboard/', views.dashboard,name='dashboard'),
    path('logout/', views.logout_link,name='logout'),
    path('add_details', views.details,name='add_details'),
    path('get_details', views.details,name='get_details'),
    path('json_page', views.json_page,name='json_page'),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
