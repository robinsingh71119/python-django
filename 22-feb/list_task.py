option = '''
    OPTION 1 : To add User(unique name)
    OPTION 2 : To View User
    OPTION 3 : To Update User
    OPTION 4 : To Delete User
    OPTION 5 : Exit
    '''
print(option)

data = []

while True:
    choice_ = input('Enter Your choice. ')
    if choice_ == '1':
        enterName = input('Enter your name. ')
        if enterName not in data:
            data.append(enterName)
        else:
            print('Name already exists')
    elif choice_ == '2':
        print(data)
    elif choice_ == '3':
        updateName = input('Enter name in the list to update. ')
        if updateName in data:
            newName = input('Enter new name to update. ')
            findIndex = data.index(updateName)
            data[findIndex] = newName
        else:
            print('Name not in list')
    elif choice_ == '4':
        updateName = input('Enter name in the list to delete. ')
        if updateName in data:
            findIndex = data.index(updateName)
            data.pop(findIndex)
        else:
            print('Invalid name')
    elif choice_ == '5':
        break
    else:
        print('Invalid Option')