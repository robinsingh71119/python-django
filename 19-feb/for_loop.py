# for i in 'python':
#     print(i)

# for i in range(11):
#     print(i)

# for i in range(5,11):
    # print(i)

# for i in range(0,20,2):
#     print(i)

li = ['red','green','blue','yellow']

# for i in li:
#     print(i)

for i in range(len(li)):
    print('index {} => value {}'.format(i,li[i]))

