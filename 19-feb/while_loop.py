# a = 0
# while a<10:
#     print(a)
#     a = a+1

# a = 9
# while a>=0:
#     print(a)
#     a = a-1

li = ['red','green','blue','yellow']

# a=0
# while a<len(li):
#     print(li[a])
#     a += 1

name_ = input('enter color to search. ')

if name_ in li:
    print('Color {} is at index = {}'.format(name_,li.index(name_)))
else:
    print('Color {} is not exists in the list'.format(name_))
