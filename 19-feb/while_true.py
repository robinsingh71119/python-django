option = '''
            Option 1: search color 
            Option 2: exit 
        '''
li = ['red','green','blue','yellow']
print(option)
while True:
    choice_ = input('Enter your option')
    if choice_ == '1':
        name_ = input('enter color to search. ')
        if name_ in li:
            print('Color {} is at index = {}'.format(name_,li.index(name_)))
        else:
            print('Color {} is not exists in the list'.format(name_))
    elif choice_ == '2':
        break
    else: print('Invalid option')
