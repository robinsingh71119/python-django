myTxt = 'Hello I am your Python Code'


# Indexing
# print(myTxt[2])
# print(myTxt[10])

# # reverse indexing
# print(myTxt[-2])


# # Slicing
# print(myTxt[2:])
# print(myTxt[:5])
# print(myTxt[:-5])

# # step size
# print(myTxt[::3])

# Membership
# print('I am a code' in myTxt)
# print('I am a code' not in myTxt)

# if 'jarvis' in myTxt:
#     print('String exists')
# else:
#     print('Not exists')

myTxt1 = ' Adding string'
myNum = 10

# concatenation
print(myTxt + myTxt1)
# print(myTxt + myNum)
