# Positional
# def student(name,email,roll_no):
#     print('Hi {} , Your Email {} and Your Roll No {}'.format(name,email,roll_no))

# student('jarvis','jar@gmail.com',101)
# student('harry','har@gmail.com',102)
# student('har@gmail.com','harry',102)
    
# Keyword 
def student(name,email,roll_no):
    print('Hi {} , Your Email {} and Your Roll No {}'.format(name,email,roll_no))

student(name='jarvis',email='jar@gmail.com',roll_no=101)
student(roll_no = 102,name='harry',email='har@gmail.com')
student('harry',roll_no=102,email='har@gmail.com')
    