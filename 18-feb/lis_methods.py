color = ['red','green','yellow','pink','gray','blue']
print(color)

# color.append('gray')
color.append('green')
color.insert(4,'violet')
color.insert(5,'green')

# update
color[2] = 'newcolor'

# color.pop()
v = color.pop(2)
color.remove('gray')
del color[0]
print(color)

print(color.count('green'))
print(len(color))

print(color.index('green'))
print(color.index('pink',1))