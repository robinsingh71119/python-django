data = {
    'name':'jarvis',
    'sname':'singh',
    'subject':('english','hindi','maths'),
    'hobbies':['playing','reading'],
    'is_present':None,
    'Technology':{
        'frontEnd':['HTML','CSS','Js'],
        'backEnd':['Python','Django'],
    }
}

# print(data['hobbies'][1])
# print(data['Technology']['frontEnd'][2])

for i in data:
    print('Key = {} : Value = {}'.format(i,data[i]))