myTxt = 'Hello I am your Python Code'
myNum = '1234567890!'
# print(myTxt.upper())
# print(myTxt.lower())
# print(myTxt.title())
# print(myTxt.capitalize())
# print(myTxt.swapcase())

# conditional fn
print(myNum.isdigit())
# print(myNum.isalpha())
print(myNum.isalnum())

# print(myTxt.isupper())
# print(myTxt.islower())
# print(myTxt.istitle())